<?php
    $target_dir = "listeprof/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "txt" ) {
        echo "Sorry, only txt files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    ?>

    

 <html>
<head>
<title>Confirmation d'upload de la liste</title>
</head>
<body>
<br>
<center>
	<h1>Confirmation d'upload de la liste</h1>
	<br>
	
	<table width=1000 height=100 border=1>
	<tr>
			<td><h2></h2>
                <form>
                <button type="submit" formaction="uploadprof.html">Revenir sur la page d'administraiton / upload</button>
                </form>
			</td>
			
	</tr>
</table>
	
</center>
</body>
</html>