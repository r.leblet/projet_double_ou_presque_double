<html>
<head>
<title>Double ou Presque Double - Test 3 / Difficile</title>
<script type="text/javascript">
 
var rep = new Array;
var faite = new Array;
var score = 0;
 

rep[1] = "c"; 
rep[2] = "c";
rep[3] = "a";
rep[4] = "b";
rep[5] = "b";
rep[6] = "c";
rep[7] = "b";
rep[8] = "a";
rep[9] = "a";
rep[10] = "c";

function Engine(question, reponse) {
        if (reponse != rep[question]) {
                if (!faite[question]) {
                        faite[question] = -1;
                        alert("Mauvaise réponse ! Votre score est de : " + score + ".  La bonne reponse est la " + rep[question]);
                        }
                else {
                        alert("Vous avez déjà répondu à cette question !");
                        }
                }
        else {
                if (!faite[question]) {
                        faite[question] = -1;
                        score++;
                        alert("Bonne réponse ! Votre score est de " + score + " point(s) !");
                        }
                else { 
                        alert("Vous avez déjà répondu à cette question !");
                        }
                }
}
 
function NextLevel () {
        if (score > 8 && score <11) {
                alert(score + "/10 " + "Bravo !") 
                self.location=""
                }
        if (score >= 6 && score <= 7) {
                alert(score + "/10 " + "Pas mal, mais il faut encore réviser la lecon !");
                }
        if (score >= 4 && score <= 5) {
                alert(score + "/10 " + "Manque d'entrainement");
                }
        if (score >= 2 && score <= 3) {
                alert(score + "/10 " + "Il faut absolument revoir les lecons des doubles / prsq double !");
                }
        if (score < 2) {
                alert(score + "/10 " + "Nous allons reprendre ensemble la lecon" );
                }
 

faite = new Array;
score = 0;
document.quest.reset();
}

</script>
</head>


<body>
<center><br>
<h1>Double ou Presque Double - Test 3 / Difficile</h1><br />

<table>
	<tr>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q1 : 33+22 </strong></span><br />
        <input onclick="Engine(1, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(1, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(1, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q2 : 28+18 </strong></span><br />
        <input onclick="Engine(2, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(2, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(2, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q3 : 27+27 </strong></span><br />
        <input onclick="Engine(3, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(3, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double <br>
		<input onclick="Engine(3, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
        
        </td>
	</tr>

</table>
<br><br>

<table>
	<tr>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q4 : 29+30 </strong></span><br />
        <input onclick="Engine(4, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(4, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(4, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q5 : 16+17 </strong></span><br />
        <input onclick="Engine(5, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(5, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(5, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q6 : 1+11 </strong></span><br />
        <input onclick="Engine(6, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(6, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double <br>
		<input onclick="Engine(6, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
        
        </td>
	</tr>

</table>
<br><br>

<table>
	<tr>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q7 : 31+32 </strong></span><br />
        <input onclick="Engine(7, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(7, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(7, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q8 : 22+22 </strong></span><br />
        <input onclick="Engine(8, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(8, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double<br>
		<input onclick="Engine(8, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
                        
	</td>
	<td>
        <span class="Style1"><span class="Style16"><strong>Q9 : 37+37 </strong></span><br />
        <input onclick="Engine(9, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(9, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double <br>
		<input onclick="Engine(9, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
        </td>
        <td>
        <span class="Style1"><span class="Style16"><strong>Q10 : 23+32 </strong></span><br />
        <input onclick="Engine(10, this.value)" value="a" name="1" type="radio"> 
                a) Double<br>
        <input onclick="Engine(10, this.value)" value="b" name="1" type="radio"> 
                b) Presque Double <br>
		<input onclick="Engine(10, this.value)" value="c" name="1" type="radio"> 
				c) Aucun des deux
        
        </td>
	</tr>

</table>
<br><br>

<table>
	<tr>
	<td>
        
                        
	</td>
	<td>
        <input name="Resultat" onclick="NextLevel()" value="Résultat" type="button">
                        
	</td>
	<td>
        
        
        </td>
        <td>
        <input type="button" value="Page précédente" onclick="history.back()">
        
        </td>
	</tr>

</table>
        

  
    
  
  

<script type="text/javascript">

var SymRealOnLoad;
var SymRealOnUnload;
var SymRealWinOpen;

function SymOnUnload()
{
  window.open = SymWinOpen;
  if(SymRealOnUnload != null)
     SymRealOnUnload();
}

function SymOnLoad()
{
  if(SymRealOnLoad != null)
     SymRealOnLoad();
  window.open = SymRealWinOpen;
  SymRealOnUnload = window.onunload;
  window.onunload = SymOnUnload;
}

SymRealOnLoad = window.onload;
window.onload = SymOnLoad;


</script>